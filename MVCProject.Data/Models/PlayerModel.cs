﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Models
{
    public class PlayerModel
    {
        public int PlayerID {get; set;}
        public string PlayerFirstName { get; set; }
        public string PlayerLastName { get; set; }
        public string PlayerPhoto { get; set; }
        public int TeamID { get; set; }
        public string TeamName { get; set; }
        public string TeamLogo { get; set; }

    }
}