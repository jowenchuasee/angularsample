﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Models
{
    public class MenuListModel
    {
        private List<MenuModel> mMenus;

        public MenuListModel(List<MenuModel> menus)
        {
            mMenus = menus;
        }

        public List<MenuModel> Menus { get
            {
                return mMenus;
            }
        }

        public bool IsDropdown(int menuId)
        {
            if (mMenus != null)
            {
                foreach (MenuModel menu in mMenus)
                {
                    if (menu.MenuParentID == menuId)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}