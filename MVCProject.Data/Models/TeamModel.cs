﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Models
{
    public class TeamModel
    {
        public int TeamID { get; set; }
        public string TeamName { get; set; }
        public string TeamAlias { get; set; }
        public string TeamShortName { get; set; }
        public string TeamLogo { get; set; }
        public int ConferenceID { get; set; }
        public string ConferenceName { get; set; }
    }
}