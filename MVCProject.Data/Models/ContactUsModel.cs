﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Models
{
    public class ContactUsModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Comments { get; set; }
    }
}