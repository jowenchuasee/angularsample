﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Models
{
    public class MenuModel
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public string MenuUrl { get; set; }
        public int MenuParentID { get; set; }
        public int MenuOrder { get; set; }
        public string Tag { get; set; }
    }
}