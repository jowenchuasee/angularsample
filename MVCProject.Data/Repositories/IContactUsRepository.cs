﻿using MVCProject.Data.Models;


namespace MVCProject.Data.Repositories
{
    public interface IContactUsRepository
    {
        string Save(ContactUsModel model);
        
    }
}