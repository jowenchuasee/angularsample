﻿using System.Collections.Generic;
using MVCProject.Data.Models;
using MVCProject.Data.Domain;
using System.Linq;

namespace MVCProject.Data.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        public IEnumerable<MenuModel> GetAllMenus()
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from m in entities.Menus
                    where m.IsDisabled == false
                    select new MenuModel
                    {
                        MenuID = m.MenuID,
                        MenuName = m.MenuName,
                        //MenuOrder=m.MenuOrder.HasValue ? m.MenuOrder.Value :0,
                        MenuParentID=m.MenuParentID,
                        MenuUrl=m.MenuUrl
                    }).ToList();
        }

    }
}
