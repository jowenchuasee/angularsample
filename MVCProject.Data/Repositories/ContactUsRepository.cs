﻿using MVCProject.Data.Models;
using MVCProject.Data.Domain;

namespace MVCProject.Data.Repositories
{
    public class ContactUsRepository : IContactUsRepository
    {
        public string Save(ContactUsModel model)
        {
            MyNBAEntities dc = new MyNBAEntities();

            ContactU entry = new ContactU();

            entry.Name = model.Name;
            entry.Email = model.Email;
            entry.Comments = model.Comments;
            dc.ContactUS.Add(entry);
            dc.SaveChanges();
            return string.Empty;
        }
        
    }
}