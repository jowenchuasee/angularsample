﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Domain;
using MVCProject.Data.Models;


namespace MVCProject.Data.Repositories
{
    public class PlayerRepository:IPlayerRepository
    {
        public IEnumerable<Player> GetAllPlayers()
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from p in entities.Players
                    select p).ToList();
        }

        public IEnumerable<PlayerModel> GetPlayers()
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from p in entities.Players
                    join t in entities.Teams
                    on p.PlayerTeamID equals t.TeamID
                    select new PlayerModel
                    {
                        PlayerID = p.PlayerID,
                        PlayerFirstName = p.PlayerFirstName,
                        PlayerLastName = p.PlayerLastName,
                        PlayerPhoto = p.PlayerPhoto,
                        TeamID = t.TeamID,
                        TeamName = t.TeamName,
                        TeamLogo = t.TeamLogo

                    }).ToList();
        }

        public IEnumerable<PlayerModel> GetPlayersByTeamId(int teamId)
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from p in entities.Players
                    join t in entities.Teams
                    on p.PlayerTeamID equals t.TeamID
                    where t.TeamID == teamId
                    select new PlayerModel
                    {
                        PlayerID = p.PlayerID,
                        PlayerFirstName = p.PlayerFirstName,
                        PlayerLastName = p.PlayerLastName,
                        PlayerPhoto = p.PlayerPhoto,
                        TeamID = t.TeamID,
                        TeamName = t.TeamName,
                        TeamLogo = t.TeamLogo

                    }).ToList();
        }

        public PlayerModel GetPlayerProfile(int playerId)
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from p in entities.Players
                    join t in entities.Teams
                    on p.PlayerTeamID equals t.TeamID
                    where p.PlayerID == playerId
                    select new PlayerModel
                    {
                        PlayerID = p.PlayerID,
                        PlayerFirstName = p.PlayerFirstName,
                        PlayerLastName = p.PlayerLastName,
                        PlayerPhoto = p.PlayerPhoto,
                        TeamID = t.TeamID,
                        TeamName = t.TeamName,
                        TeamLogo = t.TeamLogo

                    }).FirstOrDefault();
        }


    }
}