﻿using System.Collections.Generic;
using MVCProject.Data.Models;
namespace MVCProject.Data.Repositories
{
    public interface IMenuRepository
    {
        IEnumerable<MenuModel> GetAllMenus();

    }
}
