﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCProject.Data.Domain;
using MVCProject.Data.Models;
namespace MVCProject.Data.Repositories
{
    public interface ITeamRepository
    {
        IEnumerable<TeamModel> GetTeams();

    }
}
