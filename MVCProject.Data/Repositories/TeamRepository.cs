﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Domain;
using MVCProject.Data.Models;

namespace MVCProject.Data.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        public IEnumerable<TeamModel> GetTeams()
        {
            MyNBAEntities entities = new MyNBAEntities();

            return (from t in entities.Teams
                    join c in entities.Conferences
                    on t.TeamConferenceID equals c.ConferenceID
                    orderby t.TeamName
                    select new TeamModel
                    {
                        TeamID = t.TeamID,
                        TeamName = t.TeamName,
                        TeamLogo = t.TeamLogo,
                        ConferenceID = t.TeamConferenceID.HasValue ? t.TeamConferenceID.Value : 0,
                        TeamAlias = t.TeamAlias,
                        TeamShortName = t.TeamShortName,
                        ConferenceName = c.ConferenceName
                    }).ToList();
        }

        
    }
}