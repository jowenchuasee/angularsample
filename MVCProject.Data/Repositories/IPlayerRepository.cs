﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCProject.Data.Domain;
using MVCProject.Data.Models;

namespace MVCProject.Data.Repositories
{
    public interface IPlayerRepository
    {
        IEnumerable<Player> GetAllPlayers();

        IEnumerable<PlayerModel> GetPlayers();

        IEnumerable<PlayerModel> GetPlayersByTeamId(int teamId);

        PlayerModel GetPlayerProfile(int playerId);
    }
}
