﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Data.Adaptors
{
    public interface ICacheStorage
    {
        void Remove(string key);
        void Store(string key, object data);
        T Retrieve<T>(string key);
    }
}