﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCProject.Data.Models;

namespace MVCProject.Data.Services
{
    public interface ITeamService
    {
        List<TeamModel> GetAllTeams();
        TeamModel GetTeamByName(string teamName);
    }
}
