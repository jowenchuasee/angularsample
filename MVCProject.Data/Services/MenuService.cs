﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Models;
using MVCProject.Data.Repositories;

namespace MVCProject.Data.Services
{
    public class MenuService : IMenuService
    {
        private IMenuRepository mMenuRepository;
        private ITeamRepository mTeamRepository;
        public MenuService(IMenuRepository menuRepository, ITeamRepository teamRepository)
        {
            mTeamRepository = teamRepository;
            mMenuRepository = menuRepository;
        }

        private int GetTeamsMenuMenuID(List<MenuModel> menus)
        {
            if (menus != null)
            {
                foreach (MenuModel menu in menus)
                {
                    if (menu.MenuName.ToLower() == "teams")
                    {
                        return menu.MenuID;
                    }
                }
            }

            return 0;
        }
        public List<MenuModel> GetPrimaryMenus()
        {
            List<TeamModel> teams = mTeamRepository.GetTeams().ToList();

            List<MenuModel> menus = mMenuRepository.GetAllMenus().ToList();

            int teamsMenuId = GetTeamsMenuMenuID(menus);

            foreach(TeamModel team in teams)
            {
                MenuModel menu = new MenuModel();
                menu.MenuID = team.TeamID;
                menu.MenuName = team.TeamName + " "+ team.TeamAlias;
                menu.MenuParentID = teamsMenuId;
                menu.MenuUrl = string.Format("/teams/{0}/{1}", team.TeamID, team.TeamName);
                menu.Tag = team.ConferenceName;
                menus.Add(menu);
            }

            return menus;
        }
        
    }
}