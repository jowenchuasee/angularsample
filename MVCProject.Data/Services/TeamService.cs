﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Models;
using MVCProject.Data.Repositories;
using MVCProject.Data.Adaptors;

namespace MVCProject.Data.Services 
{
    public class TeamService : ITeamService
    {
        private ITeamRepository mTeamRepository;
        private ICacheStorage mCacheStorage;

        public TeamService(ITeamRepository teamRepository, ICacheStorage cacheStorage)
        {
            mTeamRepository = teamRepository;
            mCacheStorage = cacheStorage;
        }
        public List<TeamModel> GetAllTeams()
        {
            List<TeamModel> teams = mCacheStorage.Retrieve<List<TeamModel>>("cache-team");

            if (teams == null)
            {
                teams = mTeamRepository.GetTeams().ToList();
            }
            return teams;
        }

        public TeamModel GetTeamByName(string teamName)
        {
            if (string.IsNullOrEmpty(teamName))
            {
                return null;
            }

            teamName = teamName.ToLower().Trim();
            List<TeamModel> teams = GetAllTeams();
            
            return (from t in teams
                    where t.TeamName.ToLower() == teamName
                    orderby t.TeamName
                    select new TeamModel
                    {
                        TeamID = t.TeamID,
                        TeamName = t.TeamName,
                        TeamLogo = t.TeamLogo
                    }).FirstOrDefault();
        }
    }
}