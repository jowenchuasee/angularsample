﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Models;
using MVCProject.Data.Repositories;

namespace MVCProject.Data.Services
{
    public interface IPlayerService
    {
        List<PlayerModel> GetPlayers();
        List<PlayerModel> GetPlayersByTeamId(int teamId);
        PlayerModel GetPlayerProfile(int playerId);
    }
}