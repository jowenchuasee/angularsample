﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCProject.Data.Models;
using MVCProject.Data.Repositories;

namespace MVCProject.Data.Services
{
    public class PlayerService:IPlayerService
    {
        IPlayerRepository mPlayerRepository;
        public PlayerService(IPlayerRepository playerRepository)
        {
            mPlayerRepository = playerRepository;
        }

        public List<PlayerModel> GetPlayers()
        {
            return mPlayerRepository.GetPlayers().ToList();
        }

        public List<PlayerModel> GetPlayersByTeamId(int teamId)
        {
            return mPlayerRepository.GetPlayersByTeamId(teamId).ToList();
        }

        public PlayerModel GetPlayerProfile(int playerId)
        {
            return mPlayerRepository.GetPlayerProfile(playerId);
        }

    }
}