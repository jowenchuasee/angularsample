﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVCProject.Data.Models;
using MVCProject.Data.Services;

namespace MVCProject.WebAPI
{
    public class TeamWebAPIController : ApiController
    {
        ITeamService mTeamService;
        IPlayerService mPlayerService;

        public TeamWebAPIController(ITeamService teamService, IPlayerService playerService)
        {
            mTeamService = teamService;
            mPlayerService = playerService;
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // GET api/<controller>/5
        [Route ("api/getteams")]
        public List<TeamModel> GetTeams()
        {
            return mTeamService.GetAllTeams();
            
        }

        [Route ("api/getplayers")]
        public List<PlayerModel> GetPlayers(int id)
        {

            return mPlayerService.GetPlayersByTeamId(id);
        }

        [Route("api/getplayer")]
        public PlayerModel GetPlayer(int id)
        {

            return mPlayerService.GetPlayerProfile(id);
        }


        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}