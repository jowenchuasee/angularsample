﻿using System.Web;
using System.Web.Optimization;

namespace MVCProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/plugins/bootstrap/js/bootstrap.min.js",
                      "~/plugins/angular-1.4.9/angular.min.js",
                      "~/plugins/angular-1.4.9/angular-route.min.js"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/plugins/bootstrap/css/bootstrap.css",
                     "~/plugins/yamm/yamm.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/AngularMVCApp")
                    .Include("~/Scripts/angularapp.js")
                    .IncludeDirectory("~/Scripts/Controllers", "*.js")
                    );
        }
    }
}
