﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Data.Services;
using MVCProject.Data.Models;

namespace MVCProject.Controllers
{
    public class MenuController : Controller
    {
        IMenuService mMenuService;

        public MenuController(IMenuService menuService)
        {
            mMenuService = menuService;
        }
        
        [ChildActionOnly]
        public ActionResult Index()
        {
            List<MenuModel> menus = mMenuService.GetPrimaryMenus();
            MenuListModel menuList = new MenuListModel(menus);
          
            return PartialView(menuList);

        }
    }
}