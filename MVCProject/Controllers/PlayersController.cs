﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Data.Services;
using MVCProject.Data.Repositories;
using MVCProject.Data.Models;

namespace MVCProject.Controllers
{
    public class PlayersController : Controller
    {
        IPlayerService mPlayerService;

        public PlayersController(IPlayerService playerService)
        {
            mPlayerService = playerService;
            
        }
        // GET: Players
        public ActionResult Index()
        {
            
            List<PlayerModel> players = mPlayerService.GetPlayers().ToList();
            return View(players);
        }

        [Route("player/{playerid}")]
        public ActionResult Player(int playerId)
        {
            PlayerModel player = mPlayerService.GetPlayerProfile(playerId);

            if (player != null)
            {
                return View(player);
            }

            return View("Error");

        }
    }
}