﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using MVCProject.Data.Models;
using MVCProject.Data.Services;
using MVCProject.Data.Repositories;
namespace MVCProject.Controllers
{
    public class TeamsController : Controller
    {
        ITeamService mTeamService;
        IPlayerRepository mPlayerRepository;

        public TeamsController(ITeamService  teamService, IPlayerRepository playerRepository)
        {
            mTeamService = teamService;
            mPlayerRepository = playerRepository;
        }
        
        [Route("teams/{teamid}/{teamname}")]
        public ActionResult Team(int teamid, string teamname)
        {
            if (string.IsNullOrEmpty(teamname))
            {
                List<TeamModel> teams = mTeamService.GetAllTeams();

                return View(teams);
            }
            else
            {
                
                TeamModel team = mTeamService.GetTeamByName(teamname);

                if (team != null)
                {
                    List<PlayerModel> players = mPlayerRepository.GetPlayersByTeamId(team.TeamID).ToList();

                    if (players != null)
                    {
                        return View("Team", players);
                    }
                }
                return View("Error");
            }
        }
        
    }
}