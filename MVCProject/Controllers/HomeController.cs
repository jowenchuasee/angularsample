﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Data.Services;
using MVCProject.Data.Repositories;
using MVCProject.Data.Models;

namespace MVCProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Route ("error")]
        public ActionResult Error404()
        {
            return View();
                 
        }
        
    }
}