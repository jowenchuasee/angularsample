﻿var app = angular.module("teamApp", ['ngRoute']);
app.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
      $routeProvider
        .when('/player/:playerId', {
            templateUrl: '/templates/player.html',
            controller: 'playerController',
            controllerAs: 'player'
        }).when('/teams/:teamId/:teamName', {
            templateUrl: '/templates/team.html',
            controller: 'teamController',
            controllerAs: 'team'
        });

      $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
      });
  }])

app.controller("teamController", ['$route', '$routeParams', '$location', '$scope', '$http', 'teamService', 
function ($route, $routeParams, $location, $scope, $http, teamService) {

    this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;
    this.$http = $http;
    this.$scope = $scope;
    this.teamService = teamService

    teamService.getTeams.async().then(function (d) {
        $scope.allTeams = d;

    });

    if ($routeParams.teamId != null) {
        teamService.getPlayers.async($routeParams.teamId).then(function (d) {
            $scope.teamplayers = d;

        });
    }

    this.showPlayers = function showPlayers(sel) {
        var teamid = $(".select-team").val()
        var teamname = $(".select-team option:selected").text()
    
        this.$location.path( "/teams/" + teamid + "/" + teamname)

    }

}]);


app.controller('playerController', ['$scope', '$routeParams', '$http','playerService', function ($scope, $routeParams, $http, playerService) {
   
    this.name = "playerController";
    this.$routeParams = $routeParams;
    this.$http = $http
    this.playerService = playerService
    
    playerService.async($routeParams.playerId).then(function (d) {
        $scope.player = d;
       
    });
}])




app.factory("playerService", function ($http) {
    
    var playerService = {
        async: function (id) {
           
            var playerProfile 
            var promise = $http.get('/api/getplayer?id=' + id).then(function (response) {
                return response.data
            })

            return promise
        }
    }

    return playerService
})

app.factory("teamService", function ($http) {

    var getPlayers = {
        async: function (id) {
            var promise = $http.get('/api/getplayers?id=' + id).then(function (response) {
                return response.data

            })

            return promise
        }
    }

    var getTeams = {
        async: function (id) {

            var promise = $http.get('/api/getteams').then(function (response) {
                return response.data
            })

            return promise
        }
    }

    return {
        getTeams: getTeams,
        getPlayers: getPlayers
    }

    return teamService
})
